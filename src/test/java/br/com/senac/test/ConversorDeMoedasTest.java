
package br.com.senac.test;

import br.com.senac.ex3.ConversorDeMoedas;
import org.junit.Test;
import static org.junit.Assert.*;


public class ConversorDeMoedasTest {
    
    public ConversorDeMoedasTest(){
    }
    
    @Test
    public void convert25ReaisEm6e17DollarAMERICANO(){
        ConversorDeMoedas conversorDeMoedas = new ConversorDeMoedas();
        double real = 25 ; 
        
        double resultado  = conversorDeMoedas.PDolarAmericano(real);
        assertEquals(6.17, resultado , 0.01);
    }
    
    @Test
    public void convert25ReaisEm8e44DollarAUSTRALIANO(){
        ConversorDeMoedas conversorDeMoedas = new ConversorDeMoedas();
        double real = 25; 
        
        double resultado  = conversorDeMoedas.PDolarAustraliano(real);
        assertEquals(8.53, resultado , 0.01);
    }
    
     @Test
    public void convert25ReaisEm5e30Euro(){
        ConversorDeMoedas conversorDeMoedas = new ConversorDeMoedas();
        double real = 25 ; 
        
        double resultado  = conversorDeMoedas.PEuro(real);
        assertEquals(5.30, resultado , 0.01);   
    }
    
    @Test
    public void convert50ReaisEm10e61Euro(){
        ConversorDeMoedas conversorDeMoedas = new ConversorDeMoedas();
        double real = 50 ; 
        
        double resultado  = conversorDeMoedas.PEuro(real);
        assertEquals(10.61, resultado , 0.01);   
    }
    
    @Test
    public void convert50ReaisEm3DollarAMERICANO(){
        ConversorDeMoedas conversorDeMoedas = new ConversorDeMoedas();
        double real = 50 ; 
        
        double resultado  = conversorDeMoedas.PDolarAmericano(real);
        assertEquals(12.34, resultado , 0.01);
    }
    
    
    
}
