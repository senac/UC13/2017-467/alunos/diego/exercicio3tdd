
package br.com.senac.ex3;


public class ConversorDeMoedas {
    
    private  final double COTACAO_DOLLAR_US = 4.05; 
    private  final double COTACAO_DOLLAR_AU = 2.93; 
    private  final double COTACAO_EURO = 4.71 ; 
        
    public double PDolarAmericano(double valor){
        return  valor / COTACAO_DOLLAR_US; 
    }
    
     public double PDolarAustraliano(double valor){
        return valor/COTACAO_DOLLAR_AU;
    }
     
      public double PEuro(double valor){
        return valor/COTACAO_EURO;
    }
    
    
}
